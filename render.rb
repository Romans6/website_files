#!/usr/bin/env ruby

#
# gem install liquid json
#

require 'liquid'
require 'json'

template_file = File.read("template.html")
data_file = File.read("data.json")
data = JSON.parse(data_file)
@template = Liquid::Template.parse(template_file)
output = @template.render(data)

File.write("index_compiled.html", output);